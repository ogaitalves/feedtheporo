using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : Manager 
{
	public Text _scoreText;

	private int _score;
	
	void Awake()
	{
		_score = PlayerPrefs.GetInt ("HIGHSCORE", 0);
		_scoreText.text = _score.ToString();
		_soundOn = PlayerPrefsX.GetBool ("SOUND", true);

	}

	// Use this for initialization
	void Start () 
	{
		//AudioManager._instance.PlayMusic(MUSIC_CLIPS.MAIN_MENU); 
	}
	
	// Update is called once per frame
	void Update () { }

	public void TimeToFeed() {SceneManager.LoadScene ("InGame"); }	

	public void GoToLeaderboards()
	{

	}

	public void GoToTrophies()
	{

	}


	public void Share()
	{

	}
}