﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum LANGUAGES
{
	PT,
	EN,
	JP,
	NONE
}

public class LanguageManager : MonoBehaviour 
{
	public static LanguageManager _instance = null;

	private LANGUAGES _actualLanguage;

	public delegate void ChangeLanguage();
	public static event ChangeLanguage OnChangeLanguage;

	private Dictionary<string,string> _dictionary = new Dictionary<string, string> ();

	public void SetNewLanguage(LANGUAGES newLanguage)
	{
		if(_actualLanguage == newLanguage)
			return;

		_actualLanguage = newLanguage;
		UpdateLanguageDictionary ();
		OnChangeLanguage ();

	}

	private void UpdateLanguageDictionary()
	{
		
	}

	public string Translate(string key)
	{
		string value = "not found";

		if (_dictionary.ContainsKey (key)) 
			value = _dictionary [key];


		return value;
	}

	void Awake()
	{
		if(_instance == null) _instance = this;

		else if (_instance != this) Destroy (gameObject);

		DontDestroyOnLoad (this.gameObject);

		_actualLanguage = (LANGUAGES) PlayerPrefs.GetInt ("LANGUAGE", (int)LANGUAGES.NONE);
	}
	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }
}