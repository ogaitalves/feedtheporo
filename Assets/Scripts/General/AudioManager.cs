﻿using UnityEngine;
using System.Collections;

public enum SFX_CLIPS
{
	CLICK_BUTTON,
	EAT_PORO_SNAX,
	EAT_TEEMO_MUSH,
	EAT_POTION,
	NONE
}

public enum MUSIC_CLIPS
{
	MAIN_MENU,
	INGAME,
	GAME_OVER,
	NONE
}

public class AudioManager : MonoBehaviour 
{
	public static AudioManager _instance = null;

	public AudioSource _music;
	public AudioSource _sfxLayerOne;
	public AudioSource _sfxLayerTwo;

	public AudioClip[] _musicClips;
	public AudioClip[] _sfxClips;

	void Awake()
	{
		if(_instance == null) _instance = this;

		else if (_instance != this) Destroy (gameObject);

		DontDestroyOnLoad (this.gameObject);
	}

	public void PlayMusic(MUSIC_CLIPS clip)
	{
		_music.clip = _musicClips [(int)clip];
		_music.Play ();
	}

	public void PlaySFX(SFX_CLIPS clip, int layer)
	{
		if (layer == 1) 
		{
			_sfxLayerOne.clip = _sfxClips [(int)clip];
			_sfxLayerOne.Play ();
		} else
		{
			_sfxLayerTwo.clip = _sfxClips [(int)clip];
			_sfxLayerTwo.Play ();
		}
	}

	public void ToggleSound()
	{
		_sfxLayerOne.mute = _sfxLayerTwo.mute = _music.mute = !_sfxLayerOne.mute;
	}

	// Use this for initialization
	void Start () { AudioManager._instance.PlayMusic(MUSIC_CLIPS.INGAME); }
	
	// Update is called once per frame
	void Update () { }
}