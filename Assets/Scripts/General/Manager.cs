﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour {

	protected bool _soundOn;

	public delegate void ToggleSound();
	
	public static event ToggleSound OnPressedSound;
	
	public void PressedSound()
	{
		_soundOn = !_soundOn;
		PlayerPrefsX.SetBool ("SOUND", _soundOn);
		OnPressedSound ();
	}

	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }
}