﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISpriteAnimator : MonoBehaviour 
{
	public int numberofframes;
	public float framedelay;
	public bool loop;
	public Sprite[] frames;
	public Image image;
	public int index = 0;

	// Use this for initialization
	void Start () 
	{
		index = 0;
		if (loop)
			InvokeRepeating ("Animations", 0, framedelay);
		else
			StartCoroutine ("Animation");
	}

	public void Restart()
	{
		index = 0;
		if (loop)
			InvokeRepeating ("Animations", 0, framedelay);
		else
			StartCoroutine ("Animation");
	}

	void Animations()
	{
		if(index >= numberofframes) index = 0;

		Swap (index);
		index++;
	}

	IEnumerator Animation()
	{
		while (index != numberofframes) 
		{
			yield return new WaitForSeconds (framedelay);
			Swap (index);
			index++;
		}

		yield return new WaitForSeconds (framedelay);
		Swap (0);
	}

	void Swap(int index){ image.sprite = frames [index]; }

	// Update is called once per frame
	void Update () { }
}