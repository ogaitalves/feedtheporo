﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TranslationItem : MonoBehaviour 
{
	public Text _text;

	private void TranslateItem() {	_text.text = LanguageManager._instance.Translate (this.gameObject.name); }

	// Use this for initialization
	void Start () 
	{
		LanguageManager.OnChangeLanguage += TranslateItem;
		TranslateItem ();
	}
	
	// Update is called once per frame
	void Update () { }
}