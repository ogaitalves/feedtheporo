﻿using UnityEngine;
using System.Collections;
using System;

public class Level : MonoBehaviour 
{
	public float drag = 20f;

	public float delayToLaunch;

	public float[] _probs = new float[Enum.GetNames(typeof(SPAWNABLE)).Length];

	public int _pointsToThisLevel;

	public delegate void SwapLevel(Level level);
	public static SwapLevel OnSwapLevel;

	public void CheckScore(int actualScore)
	{
		if (actualScore == _pointsToThisLevel)
			OnSwapLevel (this);
	}

	// Use this for initialization
	void Start () { InGameManager.OnUpdateScore += CheckScore; }

	public GameObject ChooseSpawningItem()
	{
		float totalProbs = 0;
		SPAWNABLE item;

		foreach (float elem in _probs)
			totalProbs += elem;

		float randomPoint = UnityEngine.Random.value * totalProbs;

		for (int i = 0; i < _probs.Length; i++) 
		{
			if (randomPoint < _probs [i]) 
			{
				item = (SPAWNABLE)i;
				return Instantiate (Resources.Load ("Game/Spawnables/" + item.ToString ())) as GameObject;
			}

			else randomPoint -= _probs [i];
		}

		Debug.Log("TEMP:" + ((SPAWNABLE)UnityEngine.Random.Range (0, Enum.GetNames (typeof(SPAWNABLE)).Length)).ToString ());

		return Instantiate (Resources.Load ("Game/Spawnables/" + ((SPAWNABLE)UnityEngine.Random.Range (0, Enum.GetNames (typeof(SPAWNABLE)).Length)).ToString ())) as GameObject;
	}
	
	// Update is called once per frame
	void Update () { }
}