﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour 
{
	public static LevelManager _instance;

	public Level[] _levels;

	public Level _actualLevel;

	public delegate void ChangeLevel(float drag);
	public static event ChangeLevel OnChangeLevel;

	void Awake()
	{
		if (_instance == null)
			_instance = this;
	}

	// Use this for initialization
	void Start ()
	{ 
		Level.OnSwapLevel += UpdateLevel;
	}

	private void UpdateLevel(Level level)
	{
		_actualLevel = level;
		OnChangeLevel (level.drag);
	}

	// Update is called once per frame
	void Update () { }
}