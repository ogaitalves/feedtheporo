﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour 
{
	public Text highscoreText, scoreText; 

	private int _highScore;
	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }

	public void GenerateMenu(string score)
	{
		int scoreValue = int.Parse (score);
		int highScoreValue = PlayerPrefs.GetInt ("HIGHSCORE", 0);
		scoreText.text = score;

		if (scoreValue <= highScoreValue)
			highscoreText.text = highScoreValue.ToString ();
		else 
		{
			highscoreText.text = score;
			PlayNewHighScoreAnimation ();
			PlayerPrefs.SetInt ("HIGHSCORE", scoreValue);
		}
	}

	public void PlayNewHighScoreAnimation()
	{

	}
}