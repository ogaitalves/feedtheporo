﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InGameManager : Manager 
{
	public static InGameManager _instance;

	public GameObject _pauseMenu;

	public GameObject _endMenu;

	public Text _labelScore;

	public Player _player;
	
	private bool _isPause;

	private int _score;

	private int _healthBar;

	public int HealthBar 
	{
		get { return _healthBar; }
		set { _healthBar = value; }
	}

	private int _healthStep = 20;

	public int HealthStep 
	{
		get { return _healthStep; }
		set { _healthStep = value;	}
	}

	private bool _isOnGameOver;

	public delegate void UpdateScore (int score);
	public static UpdateScore OnUpdateScore;

	void Awake()
	{
		if (_instance == null)
			_instance = this;
	}

	public void InvokeSpawningObject()
	{
		CancelInvoke ();
		InvokeRepeating ("SpawningObject",0, LevelManager._instance._actualLevel.delayToLaunch);
	}

	public void SpawningObject()
	{
		SpawnManager._instance.SpawnObject ();
	}

	// Use this for initialization
	void Start () 
	{ 
		_isPause = false; 
		InvokeSpawningObject();
	}
	
	// Update is called once per frame
	void Update () { }

	public void PressedQuit() { SceneManager.LoadScene("MainMenu"); }

	public void PressedPause() 
	{	
		if (_isOnGameOver)
			return;

		_isPause = !_isPause;

		if (_isPause)
			Time.timeScale = 0f;
		else
			Time.timeScale = 1f;

		_pauseMenu.SetActive (!_pauseMenu.activeSelf);
	}

	public void PressedRestart()
	{
		Time.timeScale = 0f;
		SceneManager.LoadScene ("InGame");
	}

	public void PressedResume()
	{
		_pauseMenu.SetActive (!_pauseMenu.activeSelf);
		Time.timeScale = 1f;
	}

	public void DecreaseHealth()
	{
		_healthBar -= _healthStep;
		_player.PlayAnimation (ANIMATIONTYPE.MISSEDPOROSNAX);
		UpdateHealthBar ();
		if (_healthBar <= 0)
			GoToEndMenu ();
	}

	public void IncrementPoroCounter() 
	{
		_score++;
		OnUpdateScore (_score);
		_labelScore.text = _score.ToString ();
	}

	public void IncreaseHealth() 
	{
		_healthBar += _healthStep;
	}

	private void GoToEndMenu()
	{
		Time.timeScale = 0f;
		_isOnGameOver = true;
		_endMenu.SetActive (true);
		_endMenu.GetComponent<GameOverMenu> ().GenerateMenu (_score.ToString ());
	}

	private void UpdateHealthBar()
	{
		
	}
}