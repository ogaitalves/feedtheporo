﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public enum ANIMATIONTYPE
{
	POROEATING,
	POROSNAXEATEN,
	MUSHROOMEATEN,
	POTIONEATEN,
	MISSEDPOROSNAX
}
	
public class Player : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
{
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayAnimation(ANIMATIONTYPE animation)
	{
		switch (animation) 
		{
		case ANIMATIONTYPE.MISSEDPOROSNAX:
			break;
		case ANIMATIONTYPE.MUSHROOMEATEN:
			break;
		case ANIMATIONTYPE.POROSNAXEATEN:
			break;
		case ANIMATIONTYPE.POTIONEATEN:
			break;
		default:
			break;
		}

	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		Debug.Log ("OnBDragging");
	}

	public void OnDrag(PointerEventData eventData)
	{
		Debug.Log ("OnDragging");
		//this.transform.position = eventData.position;
		Vector3 temp = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 0));;
		this.transform.position = new Vector3(temp.x, this.transform.position.y, this.transform.position.z);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Debug.Log ("OnEDragging");
	}

	public void OnCollisionEnter2D(Collision2D collision) 
	{ 
		if (collision.collider.tag == "BorderLine")
			OnBorderLineCollision ();

		else OnSpawnableCollision(collision.gameObject.GetComponent<Spawnable>().type);

	}

	private void OnBorderLineCollision()
	{

	}

	private void OnSpawnableCollision(SPAWNABLE type)
	{
		switch (type) 
		{
			case SPAWNABLE.MUSHROOM: 
				PlayAnimation (ANIMATIONTYPE.MUSHROOMEATEN);
				break;

			case SPAWNABLE.POTION:
				PlayAnimation (ANIMATIONTYPE.POTIONEATEN);
				break;

		case SPAWNABLE.PORO_SNAX:
				PlayAnimation (ANIMATIONTYPE.POROSNAXEATEN);
				break;

			default:
				break;
		}
	}
}