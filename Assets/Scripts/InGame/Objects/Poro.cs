﻿using UnityEngine;
using System.Collections;

public class Poro : Spawnable 
{
	public void OnCollisionEnter2D(Collision2D collision) 
	{ 
		if (collision.collider.tag == "BorderLine")
			OnBorderLineCollision ();

		else OnPlayerCollision();

	}

	public override void OnBorderLineCollision ()
	{
		InGameManager._instance.DecreaseHealth ();
		GameObject.Destroy (this.gameObject);
	}

	public override void OnPlayerCollision ()
	{
		InGameManager._instance.IncrementPoroCounter ();
		GameObject.Destroy (this.gameObject);
	}

	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }
}