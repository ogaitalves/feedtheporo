﻿using UnityEngine;
using System.Collections;

public class Potion : Spawnable 
{
	public void OnCollisionEnter2D(Collision2D collision) 
	{ 
		if (collision.collider.tag == "BorderLine")
			OnBorderLineCollision ();

		else OnPlayerCollision();

	}

	public override void OnBorderLineCollision ()
	{
		GameObject.Destroy (this.gameObject);
	}

	public override void OnPlayerCollision ()
	{
		InGameManager._instance.IncreaseHealth ();
		base.DestroyObject ();
	}
	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }
}