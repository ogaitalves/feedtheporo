﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour 
{
	public static SpawnManager _instance;
	
	public GameObject[] _spawns = new GameObject[3];
	
	public GameObject[] _activeSpawns;

	private Dictionary<int, GameObject> _spawnerToUse = new Dictionary<int, GameObject>();
	// Use this for initialization

	void Awake()
	{
		if (_instance == null)
			_instance = this;
	}

	void Start()
	{
		for (int i = 0; i < _spawns.Length; i++)
		{
			_spawnerToUse.Add(i, _spawns[i]);
		}
	}
	
	public void AddSpawn(int id, GameObject spawner)
	{
		_spawnerToUse.Add(id, spawner);
	}
	
	public GameObject ChooseUsableSpawner()
	{
		int[] keysArray = new int[_spawnerToUse.Keys.Count];
		
		_spawnerToUse.Keys.CopyTo(keysArray, 0);
		
		int nextSpawn = keysArray[Random.Range(0, keysArray.Length)];
		
		GameObject nextSpawnToUse = _spawnerToUse[nextSpawn];
		
		//_spawnerToUse.Remove(nextSpawn);
		
		return nextSpawnToUse;
	}

	public void SpawnObject()
	{
		GameObject go = LevelManager._instance._actualLevel.ChooseSpawningItem ();
		GameObject spawn = ChooseUsableSpawner ();

		go.transform.SetParent (spawn.transform);
		go.transform.localPosition = spawn.transform.localPosition;

		StartCoroutine(ReactivateSpawn(spawn.GetComponent<Spawner>().SpawnerID, spawn));
	}

	private IEnumerator ReactivateSpawn(int id, GameObject spawn)
	{
		yield return new WaitForSeconds (LevelManager._instance._actualLevel.delayToLaunch);
		//AddSpawn (id, spawn);
	}
}