﻿using UnityEngine;
using System.Collections;

public enum SPAWNABLE
{
	POTION = 2,
	MUSHROOM = 1,
	PORO_SNAX = 0
}

public abstract class Spawnable : MonoBehaviour
{
	protected SPAWNABLE _type;

	public SPAWNABLE type
	{
		get { return _type; }
		set { _type = value; }

	}

	protected float _drag;

	public float Drag 
	{
		get { return _drag; }
		set { _drag = value; }
	}

	void Awake()
	{
		LevelManager.OnChangeLevel += UpdateValues;
	}

	void Start() { }

	private void UpdateValues(float drag)
	{
		SetNewValues (drag);
	}

	private void SetNewValues (float drag)
	{
		this.gameObject.GetComponent<Rigidbody2D> ().drag = drag;
	}
	
	public abstract void OnPlayerCollision ();

	public abstract void OnBorderLineCollision ();

	public void DestroyObject()
	{
		LevelManager.OnChangeLevel -= UpdateValues;
		GameObject.Destroy (this.gameObject);
	}

	public void OnBecameInvisible()
	{
		GameObject.Destroy (this.gameObject);
	}
		
}